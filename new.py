from flask import Flask, request
from flask_mysqldb import MySQL

app = Flask(__name__)

# MySQL configurations
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'yourusername'
app.config['MYSQL_PASSWORD'] = 'yourpassword'
app.config['MYSQL_DB'] = 'yourdatabase'
app.config['MYSQL_CURSORCLASS'] = 'DictCursor'

mysql = MySQL(app)

@app.route("/me")
def ipnew():
    try:
        with app.app_context():
            cur = mysql.connection.cursor()
            cur.execute('''
            CREATE TABLE IF NOT EXISTS new (
                id INT AUTO_INCREMENT PRIMARY KEY,
                reverse_ip TEXT
            )
            ''')
            print("Table creation query executed.")
            
            # Retrieve the client's IP address
            client_ip = request.remote_addr
            # Reverse the IP address
            reversed_ip = client_ip[::-1]
            
            # Inserting the reversed IP into the table
            cur.execute("INSERT INTO new (reverse_ip) VALUES (%s)", (reversed_ip,))
            print("Insert query executed.")
            
            mysql.connection.commit()
            print("Transaction committed.")
            
    except Exception as e:
        print(f"Error: {e}")
        return "Database operation failed", 500
    finally:
        cur.close()
        print("Cursor closed.")
        
    return f"IP {client_ip} reversed and stored as {reversed_ip}"

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port=3000)
    