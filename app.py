from flask import Flask,render_template
from flask_mysqldb import MySQL
import requests 
from mysql import connector 
import mysql.connector 
import os
from dotenv import load_dotenv

import urllib.parse
from dotenv import dotenv_values


# Load environment variables from .env file
config = dotenv_values(".env")

app=Flask(__name__)

url = urllib.parse.urlparse(config['DATABASE_URL'])
app.config['MYSQL_HOST'] = url.hostname
app.config['MYSQL_USER'] = url.username
app.config['MYSQL_PASSWORD'] = url.password
app.config['MYSQL_DB'] = url.path[1:] # Remove leading slash
app.config['MYSQL_PORT'] = url.port

# # # MySQL configuration
# app.config['MYSQL_HOST'] = 'database-01.cgcqqlhk3z2h.us-east-1.rds.amazonaws.com'  # MySQL host (e.g., localhost)
# app.config['MYSQL_USER'] = 'admin'  # MySQL username
# app.config['MYSQL_PASSWORD'] = 'password'  # MySQL password
# app.config['MYSQL_DB'] = 'kenny'  # MySQL database name

# # MySQL configuration using environment variables
# app.config['MYSQL_HOST'] = os.getenv('MYSQL_HOST')
# app.config['MYSQL_USER'] = os.getenv('MYSQL_USER')
# app.config['MYSQL_PASSWORD'] = os.getenv('MYSQL_PASSWORD')
# app.config['MYSQL_DB'] = os.getenv('MYSQL_DB')

mysql = MySQL(app)




def get_public_ip():
    response = requests.get('https://api.ipify.org').text
    return response

def reverse_ip(ip):
    segments = ip.split('.')
    reversed_ip = '.'.join(reversed(segments))
    return reversed_ip

public_ip = get_public_ip()
reversed_ip = reverse_ip(public_ip)
print(f'Public IP: {public_ip}')
print(f'Reversed IP: {reversed_ip}')


@app.route("/")
def home():
    public_ip = get_public_ip()
    reversed_ip = reverse_ip(public_ip)
    
    return render_template('index.html', reversed_ip=reversed_ip)


@app.route("/ip")
def ip():
    public_ip = get_public_ip()
    reversed_ip = reverse_ip(public_ip)
    
    try:
        cursor = mysql.connector.connect(user='admin', 
                                         password='password', 
                                         host='database-01.cgcqqlhk3z2h.us-east-1.rds.amazonaws.com', 
                                         database='kenny')
        print("Connected to the database.")
        
        # Creating the table if it doesn't exist
        cursor.execute("""
        CREATE TABLE IF NOT EXISTS devops (
            id INT AUTO_INCREMENT PRIMARY KEY,
            ip VARCHAR(15)
        )
        """)
        print("Table creation query executed.")
        
        public_ip = get_public_ip()
        reversed_ip = reverse_ip(public_ip)

        # Inserting the reversed IP into the table
        cursor.execute("INSERT INTO devops (ip) VALUES (%s)", (reversed_ip,))
        print("Insert query executed.")

        # Committing the transaction
        mysql.connection.commit()
        print("Transaction committed.")
        
        cursor.close()
        print("Cursor closed.")
    except Exception as e:
        print(f"Error: {e}")
        
    return render_template('index.html', reversed_ip=reversed_ip)

@app.route("/bug")
def bug():
    public_ip = get_public_ip()
    reversed_ip = reverse_ip(public_ip)

    cursor = connector.connect(user='admin', 
                                         password='password', 
                                         host='database-01.cgcqqlhk3z2h.us-east-1.rds.amazonaws.com', 
                                         database='kenny')
    print("Connected to the database.")
    
    # Creating the table if it doesn't exist
    cursor.execute("""
    CREATE TABLE IF NOT EXISTS devops (
        id INT AUTO_INCREMENT PRIMARY KEY,
        ip VARCHAR(15)
    )
    """)
    print("Table creation query executed.")
    
    public_ip = get_public_ip()
    reversed_ip = reverse_ip(public_ip)

    # Inserting the reversed IP into the table
    cursor.execute("INSERT INTO devops (ip) VALUES (%s)", (reversed_ip,))
    print("Insert query executed.")

    # Committing the transaction
    mysql.connection.commit()
    print("Transaction committed.")
    
    cursor.close()
    print("Cursor closed.")

        
    return render_template('index.html', reversed_ip=reversed_ip)
    

    
if __name__ == '__main__':
    app.run(debug=True, port=3000,host="0.0.0.0")


# from flask import Flask,render_template
# from flask_mysqldb import MySQL
# import requests  


# app=Flask(__name__)

# #find the hostname and machine ip

# # MySQL configuration
# app.config['MYSQL_HOST'] = ''  # MySQL host (e.g., localhost)
# app.config['MYSQL_USER'] = ''  # MySQL username
# app.config['MYSQL_PASSWORD'] = ''  # MySQL password
# app.config['MYSQL_DB'] = ''  # MySQL database name

# mysql = MySQL(app)

# def get_public_ip():
#     response = requests.get('https://api.ipify.org').text
#     return response

# def reverse_ip(ip):
#     segments = ip.split('.')
#     reversed_ip = '.'.join(reversed(segments))
#     return reversed_ip

# public_ip = get_public_ip()
# reversed_ip = reverse_ip(public_ip)
# print(f'Public IP: {public_ip}')
# print(f'Reversed IP: {reversed_ip}')

# @app.route("/")
# def home():
#     public_ip = get_public_ip()
#     reversed_ip = reverse_ip(public_ip)
    
#     # Storing the reversed IP in the database
#     cursor = mysql.connection.cursor()
#     cursor.execute("CREATE TABLE IF NOT EXISTS reversed_ips (id INT AUTO_INCREMENT PRIMARY KEY, ip VARCHAR(15))")
#     cursor.execute("INSERT INTO reversed_ips (ip) VALUES (%s)", (reversed_ip,))
#     mysql.connection.commit()
#     cursor.close()
    
#     return render_template('index.html', reversed_ip=reversed_ip)
    
    
# if __name__ == '__main__':
#     app.run(debug=True, port=3000,host="0.0.0.0")