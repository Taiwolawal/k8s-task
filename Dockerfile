# Use the official Python image from the Docker Hub
FROM python:3.10-slim-bullseye

# Set the working directory in the container
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY . /app

# Copy the current directory contents into the container at /app
COPY .env /app 

RUN mv -i /etc/apt/trusted.gpg.d/debian-archive-*.asc  /root/
RUN ln -s /usr/share/keyrings/debian-archive-* /etc/apt/trusted.gpg.d/

# Install system dependencies
RUN apt-get update && apt-get install -y \
    libmariadb-dev \
    gcc \
    build-essential \
    pkg-config \
    default-mysql-client \
    && rm -rf /var/lib/apt/lists/*

# RUN pip install python-dotenv

# Install any needed packages specified in requirements.txt
RUN pip install --upgrade pip
RUN pip install --no-cache-dir -r requirements.txt

# Make port 3000 available to the world outside this container
EXPOSE 3000

# Run app.py when the container launches
CMD ["python3","app.py"]



# FROM python:alpine
# ADD . /app
# WORKDIR /app
# COPY requirements.txt .
# RUN pip install --upgrade pip
# RUN pip install -r requirement.txt
# COPY . .
# EXPOSE 5002
# ENTRYPOINT ["python","app.py"]

# # Use the official Python image from the Docker Hub
# FROM python:3.10-slim

# # Set the working directory in the container
# WORKDIR /app

# # Copy the current directory contents into the container at /app
# COPY . /app

# # Install system dependencies
# # RUN apt-get update && apt-get install -y \
# #     # build-essential \
# #     # libmariadb-dev \
# #     # pkg-config \
# #     # && rm -rf /var/lib/apt/lists/*

# # Upgrade pip
# RUN pip install --upgrade pip

# # Install any needed packages specified in requirements.txt
# RUN pip install --no-cache-dir -r requirements.txt

# # Make port 3000 available to the world outside this container
# EXPOSE 3000

# # Define environment variable
# ENV FLASK_APP=app.py

# # Run app.py when the container launches
# CMD ["flask", "run", "--host=0.0.0.0", "--port=3000"]


# ###
# # Use the official Python image from the Docker Hub
# FROM python:3.10-slim

# # Set the working directory in the container
# WORKDIR /app

# # Copy the current directory contents into the container at /app
# COPY . /app

# # Install system dependencies
# RUN apt-get update && apt-get install -y \
#     build-essential \
#     libmariadb-dev \
#     default-mysql-client \
#     && rm -rf /var/lib/apt/lists/*

# # Install any needed packages specified in requirements.txt
# RUN pip install --upgrade pip
# RUN pip install --no-cache-dir -r requirements.txt

# # Make port 3000 available to the world outside this container
# EXPOSE 3000CMD ["python","app.py"]

# # # Define environment variable
# # ENV FLASK_APP=app.py

# # Run app.py when the container launches


# ####
# FROM python:3.9-slim-buster
# WORKDIR /app
# COPY ./requirements.txt /app
# RUN pip install -r requirements.txt
# COPY . .
# EXPOSE 5000
# ENV FLASK_APP=my_flask.py
# CMD ["flask", "run", "--host", "0.0.0.0"]

# sudo apt-get update
# sudo apt-get install -y pkg-config libmariadb-dev
# pip3 install Flask-MySQLdb
# Werkzeug