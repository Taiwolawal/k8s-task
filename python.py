from flask import Flask,render_template
from flask_mysqldb import MySQL
import requests  


app=Flask(__name__)

#find the hostname and machine ip

# MySQL configuration
app.config['MYSQL_HOST'] = 'database-01.cgcqqlhk3z2h.us-east-1.rds.amazonaws.com'  # MySQL host (e.g., localhost)
app.config['MYSQL_USER'] = 'admin'  # MySQL username
app.config['MYSQL_PASSWORD'] = 'password'  # MySQL password
app.config['MYSQL_DB'] = 'taiwo'  # MySQL database name

mysql = MySQL(app)


def get_public_ip():
    response = requests.get('https://api.ipify.org').text
    return response

def reverse_ip(ip):
    segments = ip.split('.')
    reversed_ip = '.'.join(reversed(segments))
    return reversed_ip

public_ip = get_public_ip()
reversed_ip = reverse_ip(public_ip)
print(f'Public IP: {public_ip}')
print(f'Reversed IP: {reversed_ip}')

@app.route("/")
def home():
    public_ip = get_public_ip()
    reversed_ip = reverse_ip(public_ip)
    
    # Storing the reversed IP in the database
    cursor = mysql.connection.cursor()
    cursor.execute("CREATE TABLE IF NOT EXISTS reversed_ips (id INT AUTO_INCREMENT PRIMARY KEY, ip VARCHAR(15))")
    cursor.execute("INSERT INTO reversed_ips (ip) VALUES (%s)", (reversed_ip,))
    mysql.connection.commit()
    cursor.close()
    
    return render_template('index.html', reversed_ip=reversed_ip)
    
    
if __name__ == '__main__':
    app.run(debug=True, port=3000,host="0.0.0.0")